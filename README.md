# Projeto Rota


## Disciplina
Tópicos Avançados em Banco de Dados


## Equipe
- Ana Cirineu
- Carlos Henrique Monteiro
- Douglas Nishiama
- Isabela Bragion Pereira
- Julio Rangel
- Leonardo Lins
- Marcelo Augusto dos Santos
- Renato Borges Gallo Junior
- Valdir Evaristo da Silva Junior

## Objetivo
Este projeto tem por objetivo criar um produto final que seja capaz de ajudar motoristas\
de vans a administrarem suas viagens, utilizando SCRUM em seu desenvolvimento.

## Tecnologias utilizadas
- Dart/Flutter
- Nodejs
- MongoDB 

### Entrega 1
[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-1)
- Planejamento de arquitetura projeto
- Criação do database MongoDB
- Prototipar aplicação
- Função da API que cadastra destino do motorista
- Interface que cadastra destino

### Entrega 2
  [Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-2)
- Motorista gerenciar passageiro e consultar rota
- Passageiro pedir embarque/desembarque

### Entrega 3
[Detalhes](https://gitlab.com/g2topicosavancados/leiame/-/wikis/Sprint-3)
- Passageiro se inscrever em viagens
- Refatoração de estrutura de navegação front-end

### Entrega 4
- Melhoria no sistema de comunicação motorista/passageiro trazendo a função de\
alteração da previsão de embarque ou desembarque.

### Entrega 5
- Adicionando a API de geolocalização para melhorar o sistema de definição de rotas.

### Entrega 6
- Implementação completa de testes, afim de garantir uma cobertura prevista casos de\
erro.
- Refatoração geral e revisão.

### Entrega 7
- Apresentação final

